import * as express from 'express';
import * as cors from 'cors';
import * as Joi from 'joi';
import * as HttpStatus from 'http-status-codes';
import * as swaggerUI from 'swagger-ui-express';
import * as swaggerDocument from './documentation.json';
import * as db from './db';
import { loadUser } from './load-user';

const app = express();
app.use(express.json());
app.use(cors());

const customSwaggerOptions = {
    explorer: true,
    swaggerOptions: {
        // TODO: this should be working
        authAction: {
            default: {
                name: 'default',
                schema: {
                    type: 'apiKey',
                    in: 'header',
                    name: 'Authorization',
                    description: ''
                },
                value: 'Bearer <my own default token>'
            }
        }
    }
};
app.use('/documentation', swaggerUI.serve, swaggerUI.setup(swaggerDocument, customSwaggerOptions));

app.post('/account/create', async (request, response) => {
    type BodySchema = {
        username: string,
        password: string,
        email: string,
    };
    const bodySchema = Joi.object().keys({
        username: Joi.string().required(),
        password: Joi.string().required(),
        email: Joi.string().required(),
    });

    let body: BodySchema;

    try {
        body = await Joi.validate<BodySchema>(request.body, bodySchema);
    } catch (error) {
        return response.status(HttpStatus.BAD_REQUEST).json();
    }

    try {
        await db.createUser(body.email, body.username, body.password);
    } catch (error) {
        if (error instanceof db.UsernameAlreadyBeingUsedError) {
            return response.status(HttpStatus.CONFLICT).json();
        }
        return response.status(HttpStatus.INTERNAL_SERVER_ERROR).json();
    }

    return response.json();
});

app.post('/account/authenticate', async (request, response) => {
    type BodySchema = {
        username: string,
        password: string,
    };
    const bodySchema = Joi.object().keys({
        username: Joi.string().required(),
        password: Joi.string().required(),
    });

    let body: BodySchema;

    try {
        body = await Joi.validate<BodySchema>(request.body, bodySchema);
    } catch (error) {
        return response.status(HttpStatus.BAD_REQUEST).json();
    }

    let authenticationToken: db.AuthenticationToken;

    try {
        authenticationToken = await db.authenticate(body.username, body.password);
    } catch (error) {
        if (error instanceof db.UserDoesNotExistError) {
            return response.status(HttpStatus.NOT_FOUND).json();
        }
        if (error instanceof db.InvalidPasswordError) {
            return response.status(HttpStatus.UNAUTHORIZED).json();
        }
        return response.status(HttpStatus.INTERNAL_SERVER_ERROR).json();
    }

    return response.json(authenticationToken);
});

app.post('/account/deauthenticate', loadUser(true), async (request, response) => {
    try {
        await db.deauthenticate(request.user!.authenticationTokenHash);
    } catch (error) {
        return response.status(HttpStatus.INTERNAL_SERVER_ERROR).json();
    }

    return response.json();
});

app.post('/applications', loadUser(true), async (request, response) => {
    type BodySchema = {
        name: string,
        code: string,
    };
    const bodySchema = Joi.object().keys({
        name: Joi.string().required(),
        code: Joi.string().required().allow(''),
    });

    let body: BodySchema;

    try {
        body = await Joi.validate<BodySchema>(request.body, bodySchema);
    } catch (error) {
        return response.status(HttpStatus.BAD_REQUEST).json();
    }

    try {
        await db.createApplication(request.user!.username, body.name, body.code);
    } catch (error) {
        if (error instanceof db.ApplicationNameAlreadyBeingUsedError) {
            return response.status(HttpStatus.CONFLICT).json();
        }
        return response.status(HttpStatus.INTERNAL_SERVER_ERROR).json();
    }

    return response.json();
});

app.patch('/applications/:name', loadUser(true), async (request, response) => {
    type ParamsSchema = {
        name: string,
    };
    const paramsSchema = Joi.object().keys({
        name: Joi.string().required(),
    });
    type BodySchema = {
        name?: string,
        code?: string,
    };
    const bodySchema = Joi.object().keys({
        name: Joi.string().optional(),
        code: Joi.string().optional(),
    });

    let params: ParamsSchema;
    let body: BodySchema;

    try {
        params = await Joi.validate<ParamsSchema>(request.params, paramsSchema);
        body = await Joi.validate<BodySchema>(request.body, bodySchema);
    } catch (error) {
        return response.status(HttpStatus.BAD_REQUEST).json();
    }

    try {
        await db.updateApplication(request.user!.username, params.name, body);
    } catch (error) {
        if (error instanceof db.ApplicationDoesNotExistError) {
            return response.status(HttpStatus.NOT_FOUND).json();
        }
        if (error instanceof db.ApplicationNameAlreadyBeingUsedError) {
            return response.status(HttpStatus.CONFLICT).json();
        }
        return response.status(HttpStatus.INTERNAL_SERVER_ERROR).json();
    }

    return response.json();
});

app.delete('/applications/:name', loadUser(true), async (request, response) => {
    type ParamsSchema = {
        name: string,
    };
    const paramsSchema = Joi.object().keys({
        name: Joi.string().required(),
    });

    let params: ParamsSchema;

    try {
        params = await Joi.validate<ParamsSchema>(request.params, paramsSchema);
    } catch (error) {
        return response.status(HttpStatus.BAD_REQUEST).json();
    }

    try {
        await db.removeApplication(request.user!.username, params.name);
    } catch (error) {
        if (error instanceof db.ApplicationDoesNotExistError) {
            return response.status(HttpStatus.NOT_FOUND).json();
        }
        return response.status(HttpStatus.INTERNAL_SERVER_ERROR).json();
    }

    return response.json();
});

app.get('/applications', loadUser(false), async (request, response) => {
    type QuerySchema = {
        username?: string,
    };
    const querySchema = Joi.object().keys({
        username: Joi.string().optional(),
    });

    let query: QuerySchema;

    try {
        query = await Joi.validate<QuerySchema>(request.query, querySchema);
    } catch (error) {
        return response.status(HttpStatus.BAD_REQUEST).json();
    }

    if (!query.username && !request.user) {
        return response.status(HttpStatus.UNAUTHORIZED).json();
    }

    let applications: db.Application[];

    try {
        applications = await db.getApplications(query.username || request.user!.username);
    } catch (error) {
        return response.status(HttpStatus.INTERNAL_SERVER_ERROR).json();
    }

    return response.json(applications);
});

app.get('/applications/:name', loadUser(false), async (request, response) => {
    type ParamsSchema = {
        name: string,
    };
    const paramsSchema = Joi.object().keys({
        name: Joi.string().required(),
    });

    type QuerySchema = {
        username?: string,
    };
    const querySchema = Joi.object().keys({
        username: Joi.string().optional(),
    });

    let params: ParamsSchema;
    let query: QuerySchema;

    try {
        params = await Joi.validate<ParamsSchema>(request.params, paramsSchema);
        query = await Joi.validate<QuerySchema>(request.query, querySchema);
    } catch (error) {
        return response.status(HttpStatus.BAD_REQUEST).json();
    }

    if (!query.username && !request.user) {
        return response.status(HttpStatus.UNAUTHORIZED).json();
    }

    let application: db.Application | null;

    try {
        application = await db.getApplication(query.username || request.user!.username, params.name);
    } catch (error) {
        return response.status(HttpStatus.INTERNAL_SERVER_ERROR).json();
    }

    if (!application) {
        return response.status(HttpStatus.NOT_FOUND).json();
    }

    return response.json(application);
});

db.connect()
    .then(() => {
        console.log('conectado ao banco de dados com sucesso');

        app.listen(8080, () => {
            console.log('escutando na porta 8080');
        });
    })
    .catch(() => {
        console.error('ocorreu um erro ao tentar se conectar com o banco de dados');
    });
