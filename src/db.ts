import * as uuid from 'uuid/v4';
import { MongoClient, Db, Collection, FilterQuery } from 'mongodb';

let client: MongoClient | undefined;
let applications: Collection<Application> | undefined;
let users: Collection<User> | undefined;
let authenticationTokens: Collection<AuthenticationToken> | undefined;

export async function connect(): Promise<void> {
    if (client) return;

    const url = 'mongodb://localhost:27017';

    client = await MongoClient.connect(url, { useNewUrlParser: true });
    const db = client.db('applications-server');
    db.collection<User>('users');

    applications = db.collection('applications');
    users = db.collection('users');
    authenticationTokens = db.collection('authentication-tokens');
}

export async function disconnect(): Promise<void> {
    if (!client) return;

    await client.close();
    client = undefined;
}

export type Application = {
    ownerUsername: string,
    name: string,
    code: string,
    lastUpdated: Date,
};

export type User = {
    email: string,
    username: string,
    password: string,
};

export type AuthenticationToken = {
    creationDate: Date,
    hash: string,
    userUsername: string,
};

export class UsernameAlreadyBeingUsedError {}
export class UserDoesNotExistError {}
export class InvalidPasswordError {}
export class ApplicationNameAlreadyBeingUsedError {}
export class ApplicationDoesNotExistError {}

function stripUnderscoreId<T extends {}>(value: T): T;
function stripUnderscoreId<T extends {}>(value: T | null): T | null;
function stripUnderscoreId<T extends {}>(value: T[]): T[];
function stripUnderscoreId<T extends {}>(value: T[] | null): T[] | null;
function stripUnderscoreId<T extends {}>(value: T | T[] | null): T | T[] | null {
    if (value === null) return null;
    if (Array.isArray(value)) return value.map(stripUnderscoreId as any);
    return { ...value, _id: undefined };
}

export async function createUser(email: string, username: string, password: string): Promise<User> {
    const isUsernameBeingUsed = await users!.findOne({ username });

    if (isUsernameBeingUsed) {
        throw new UsernameAlreadyBeingUsedError();
    }

    const user: User = { email, username, password };
    await users!.insertOne(user);

    return stripUnderscoreId(user);
}

export async function authenticate(username: string, password: string): Promise<AuthenticationToken> {
    const user = await users!.findOne({ username });

    if (!user) {
        throw new UserDoesNotExistError();
    }

    if (user.password !== password) {
        throw new InvalidPasswordError();
    }

    const authenticationToken: AuthenticationToken = {
        creationDate: new Date(),
        userUsername: username,
        hash: uuid(),
    };
    await authenticationTokens!.insertOne(authenticationToken);

    return stripUnderscoreId(authenticationToken);
}

export async function deauthenticate(authenticationTokenHash: string): Promise<void> {
    await authenticationTokens!.deleteOne({ hash: authenticationTokenHash });
}

export async function findUserByAuthenticationTokenHash(authenticationTokenHash: string): Promise<User | undefined> {
    const authenticationToken = await authenticationTokens!.findOne({ hash: authenticationTokenHash });

    if (!authenticationToken) return undefined;

    const user = await users!.findOne({ username: authenticationToken.userUsername });

    if (!user) {
        console.error(`Usuário com nome "${authenticationToken.userUsername}" autenticou porém não existe.`);
        return undefined;
    }

    return stripUnderscoreId(user);
}

export async function createApplication(userUsername: string, applicationName: string, applicationCode: string): Promise<Application> {
    const existingApplication = await applications!.findOne({ ownerUsername: userUsername, name: applicationName });

    if (existingApplication) throw new ApplicationNameAlreadyBeingUsedError();

    const application: Application = {
        ownerUsername: userUsername,
        name: applicationName,
        code: applicationCode,
        lastUpdated: new Date(),
    };
    await applications!.insertOne(application);

    return stripUnderscoreId(application);
}

export async function removeApplication(userUsername: string, applicationName: string): Promise<Application> {
    const query: FilterQuery<Application> = { ownerUsername: userUsername, name: applicationName };
    const application = await applications!.findOne(query);

    if (!application) {
        throw new ApplicationDoesNotExistError();
    }

    await applications!.deleteOne(query);

    return stripUnderscoreId(application);
}

export async function getApplications(userUsername: string): Promise<Application[]> {
    const sort: Partial<Record<keyof Application, number>> = { lastUpdated: -1 };
    return stripUnderscoreId(await applications!.find({ ownerUsername: userUsername }).sort(sort).toArray());
}

export async function getApplication(userUsername: string, applicationName: string): Promise<Application | null> {
    const application = await applications!.findOne({ ownerUsername: userUsername, name: applicationName });
    return stripUnderscoreId(application);
}

export async function updateApplication(userUsername: string, applicationName: string, changes: Partial<Pick<Application, 'name' | 'code'>>) {
    const query: FilterQuery<Application> = { ownerUsername: userUsername, name: applicationName };
    const application = await applications!.findOne(query);

    if (!application) throw new ApplicationDoesNotExistError();

    if (changes.name !== undefined && changes.name !== applicationName) {
        const existingApplication = await applications!.findOne({ ownerUsername: userUsername, name: changes.name });

        if (existingApplication) throw new ApplicationNameAlreadyBeingUsedError();
    }

    await applications!.replaceOne({ ownerUsername: userUsername, name: applicationName }, { ...application, ...changes, lastUpdated: new Date() });
}
