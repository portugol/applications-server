import * as express from 'express';
import * as HttpStatus from 'http-status-codes';
import { User, findUserByAuthenticationTokenHash } from './db';

declare global {
    namespace Express {
        interface Request {
            user?: User & { authenticationTokenHash: string };
        }
    }
}

export const loadUser: (authenticationRequired: boolean) => express.Handler = (authenticationRequired) => async (request, response, next) => {
    const headerAuthorization = request.headers['authorization'];

    let authenticationTokenHash: string = '';
    let user: User | undefined;

    if (headerAuthorization) {
        authenticationTokenHash = headerAuthorization.split('Bearer ')[1];

        if (authenticationTokenHash) {
            user = await findUserByAuthenticationTokenHash(authenticationTokenHash);
        }
    }

    if (!user && authenticationRequired) return response.status(HttpStatus.UNAUTHORIZED).json();

    if (user) {
        request.user = { ...user, authenticationTokenHash };
    }
    next();
};
